<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Allrma extends Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_allrma';
        $this->_blockGroup = 'Aspl_Rmasystem';
        $this->_headerText = __('Manage RMA');
        parent::_construct();
        $this->buttonList->remove('add');
    }
}
