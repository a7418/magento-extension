<?php

namespace Aspl\Rmasystem\Block\View\Element\Html;

class Link extends \Magento\Framework\View\Element\Html\Link
{
    /**
     * @var \Aspl\Rmasystem\Helper\Data
     */
    private $helper;

 
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Aspl\Rmasystem\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->helper->isCustomerLoggedIn()) {
            if (false != $this->getTemplate()) {
                return parent::_toHtml();
            }
            return '<li><a ' . $this->getLinkAttributes() . ' >' . $this->escapeHtml($this->getLabel()) . '</a></li>';
        }
    }

    /**
     * Get href URL
     *
     * @return string
     */
    public function getHref()
    {
        return $this->getUrl('rmasystem/guest/login/');
    }

    public function isCustomerLoggedIn()
    {
        return $this->helper->isCustomerLoggedIn();
    }
}
