/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
require([
    'jquery',
], function (
    $
) {
    $(document).ready(function () {
        if (jQuery(".delete-image").length) {
            jQuery(".delete-image").remove();
        } else {
            jQuery("#shippinglabel_filename").addClass("required-entry");
        }

    });
});