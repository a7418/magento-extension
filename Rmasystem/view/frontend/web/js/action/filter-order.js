
define(
    [
        'jquery',
        'mage/url',
    ],
    function ($, urlBuilder) {
        'use strict';

        return function (filterData) {
            var serviceUrl,
                payload;

            var contentType = 'application/json';
            console.log(filterData)
            /**
             * Checkout for guest and registered customer.
             */
            serviceUrl = 'rest/V1/apply/filter/';
            payload = {
                orderId: filterData.orderFilter,
                price: filterData.priceFilter,
                date: filterData.dateFilter
            };
            return $.ajax({
                url: urlBuilder.build(serviceUrl),
                type: 'POST',
                showLoader: true,
                data: JSON.stringify(payload),
                global: true,
                contentType: contentType

            });
        };
    }
);
