
define(
    [
        'jquery',
        'mage/url',
    ],
    function ($, urlBuilder) {
        'use strict';

        return function (orderData) {
            var serviceUrl,
                payload;

            var contentType = 'application/json';

            /**
             * Checkout for guest and registered customer.
             */
            serviceUrl = 'rest/V1/customer/order-details';
            payload = {
                orderId: orderData.entity_id
            };
            // console.log(payload);
            // die();
            return $.ajax({
                url: urlBuilder.build(serviceUrl),
                type: 'POST',
                showLoader: true,
                data: JSON.stringify(payload),
                global: true,
                contentType: contentType
            });
        };
    }
);
