var config = {
    map: {
        '*': {
            newRma:  'Aspl_Rmasystem/js/new-rma',
            rmaList: 'Aspl_Rmasystem/js/rma-list',
            footerLinkJs: 'Aspl_Rmasystem/js/footerLinkJs'
        }
    },
    paths: {
         'myjs': "Aspl_Rmasystem/js/address"
     
      },
    shim: {
        'myjs': {
            deps: ['jquery']
    }
    }
};
