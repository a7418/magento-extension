<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Controller\Newrma;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Aspl\Rmasystem\Helper\Data $helperData,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->helperData = $helperData;
        $this->_messageManager = $messageManager;
        parent::__construct($context);
    }

    /**
     *
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $isModuleEnable = $this->helperData->getConfigData('enable');
        if (!$isModuleEnable) {
            $this->_messageManager->addWarning(__('Requested url not found.'));
            $this->_redirect('/');
        }
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('New RMA'));

        $navigationBlock = $resultPage->getLayout()->getBlock('customer_account_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('rmasystem');
        }
        return $resultPage;
    }
}
