<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Controller;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\App\Filesystem\DirectoryList;
use Aspl\Rmasystem\Model\ResourceModel\Rmaitem\CollectionFactory as ItemCollectionFactory;
use Magento\Sales\Model\OrderRepository;
use Aspl\Rmasystem\Api\AllRmaRepositoryInterface;
use Aspl\Rmasystem\Api\Data\AllrmaInterfaceFactory;
use Magento\Framework\Session\SessionManager;
use Magento\ImportExport\Model\ResourceModel\Helper as ResourceModelHelper;

class GuestFrontController extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Aspl\Rmasystem\Helper\Email
     */
    protected $_emailHelper;

    /**
     * @var \Aspl\Rmasystem\Helper\Data
     */
    protected $helper;

    /**
     * @var File
     */
    protected $_fileIo;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * File Uploader factory.
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * @var ItemCollectionFactory
     */
    protected $itemCollectionFactory;

    /**
     * @var \Aspl\Rmasystem\Api\Data\RmaitemInterfaceFactory
     */
    protected $rmaItemDataFactory;

    /**
     * @var \Aspl\Rmasystem\Api\RmaitemRepositoryInterface
     */
    protected $rmaItemRepository;

    /**
     * @var \Aspl\Rmasystem\Api\Data\ConversationInterfaceFactory
     */
    protected $conversationDataFactory;

    /**
     * @var \Aspl\Rmasystem\Api\ConversationRepositoryInterface
     */
    protected $conversationRepository;

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var AllRmaRepositoryInterface
     */
    protected $rmaRepository;

    /**
     * @var AllrmaInterfaceFactory
     */
    protected $rmaFactory;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ResourceModelHelper
     */
    protected $resourceModelHelper;

    /**
     * @var \Aspl\Rmasystem\Api\Data\ReasonRepositoryInterface
     */
    protected $reasonRepository;

    /**
     * @var [type]
     */
    protected $fieldValue;
    /**
     * @param Context                                                 $context
     * @param Session                                                 $customerSession
     * @param AsplRmasystemHelperEmail                              $emailHelper
     * @param AsplRmasystemHelperData                               $helper
     * @param MagentoFrameworkFilesystem                              $filesystem
     * @param Magento\Media\Storage\Model\FileUploaderFactory         $fileUploaderFactory
     * @param ItemCollectionFactory                                   $itemCollectionFactory
     * @param Aspl\Rmasystem\Api\Data\RmaitemInterfaceFactory       $rmaItemDataFactory
     * @param Aspl\Rmasystem\Api\RmaitemRepositoryInterface         $rmaItemRepository
     * @param Aspl\Rmasystem\Api\Data\ConversationInterfaceFactory  $conversationDataFactory
     * @param Aspl\Rmasystem\Api\ConversationRepositoryInterface    $conversationRepository
     * @param OrderRepository                                         $orderRepository
     * @param AllRmaRepositoryInterface                               $rmaRepository
     * @param AllrmaInterfaceFactory                                  $rmaFactory
     * @param ResourceModelHelper                                     $resourceModelHelper
     * @param SessionManager                                          $session
     * @param File                                                    $fileIo
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        \Aspl\Rmasystem\Helper\Email $emailHelper,
        \Aspl\Rmasystem\Helper\Data $helper,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        ItemCollectionFactory $itemCollectionFactory,
        \Aspl\Rmasystem\Api\Data\RmaitemInterfaceFactory $rmaItemDataFactory,
        \Aspl\Rmasystem\Api\RmaitemRepositoryInterface $rmaItemRepository,
        \Aspl\Rmasystem\Api\Data\ConversationInterfaceFactory $conversationDataFactory,
        \Aspl\Rmasystem\Api\ConversationRepositoryInterface $conversationRepository,
        OrderRepository $orderRepository,
        AllRmaRepositoryInterface $rmaRepository,
        AllrmaInterfaceFactory $rmaFactory,
        ResourceModelHelper $resourceModelHelper,
        SessionManager $session,
        File $fileIo,
        \Aspl\Rmasystem\Model\FieldvalueFactory $fieldValueFactory,
        \Aspl\Rmasystem\Api\ReasonRepositoryInterface $reasonRepository
    ) {

        $this->_customerSession = $customerSession;
        $this->_fileIo = $fileIo;
        $this->_emailHelper = $emailHelper;
        $this->helper = $helper;
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->itemCollectionFactory = $itemCollectionFactory;
        $this->rmaItemDataFactory = $rmaItemDataFactory;
        $this->rmaItemRepository = $rmaItemRepository;
        $this->conversationDataFactory = $conversationDataFactory;
        $this->conversationRepository = $conversationRepository;
        $this->orderRepository = $orderRepository;
        $this->rmaRepository = $rmaRepository;
        $this->rmaFactory = $rmaFactory;
        $this->resourceModelHelper = $resourceModelHelper;
        $this->session = $session;
        $this->fieldValue = $fieldValueFactory;
        $this->reasonRepository = $reasonRepository;
        parent::__construct($context);
    }
    
    public function execute()
    {
      /** child controllers will use it */
    }

    protected function saveRmaProductImage($numberOfImages, $lastRmaId)
    {
        $imageArray = [];
        try {
            if ($numberOfImages > 0) {
                $path = $this->helper->getBaseDir($lastRmaId);
                for ($i = 0; $i < $numberOfImages; $i++) {
                    $fileId = "related_images[$i]";
                    $this->uploadImage($fileId, $path, $imageArray);
                }
            }
            return $imageArray;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Notify message that rma created.
     * @param  int $rmaId
     */
    public function saveRmaHistory($rmaId, $message)
    {
        $conversationModel = $this->conversationDataFactory->create()
          ->setRmaId($rmaId)
          ->setMessage($message)
          ->setCreatedAt(time())
          ->setSender('default');
        try {
            $this->conversationRepository->save($conversationModel);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
            return $resultRedirect->setPath('*/*/index');
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $this->messageManager->addException($e, __('Something went wrong while saving the Message.'));
            return $resultRedirect->setPath('*/*/index');
        }
    }

    /**
     * Upload Image of Rma
     *
     * @param string $fileId
     * @param string $uploadPath
     * @param int $count
     */
    protected function uploadImage($fileId, $path, &$imageArray)
    {
        $extArray = ['jpg','jpeg','gif','png', 'svg'];
        try {
            /** @var $uploader \Magento\MediaStorage\Model\File\Uploader */
            $uploader = $this->_fileUploaderFactory->create(['fileId' => $fileId]);
            $uploader->setAllowedExtensions($extArray);
            $uploader->setAllowRenameFiles(true);
            $uploader->setAllowCreateFolders(true);
            $result = $uploader->save($path);
            $imageArray[$result['file']] = $result['file'];
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $this->messageManager->addException($e, __($e->getMessage()));
            return $e->getMessage();
        }
    }
    public function getSerilizeData($data)
    {
        return  $this->helper->getSerializeData($data);
    }
}
