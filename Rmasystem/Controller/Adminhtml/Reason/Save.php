<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Controller\Adminhtml\Reason;

use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

class Save extends \Magento\Backend\App\Action
{

   /**
    * @var \Aspl\Rmasystem\Api\ReasonInterfaceFactory
    */
    protected $reasonDataFactory;

    /**
     * @var \Aspl\Rmasystem\Api\Data\ReasonRepositoryInterface
     */
    protected $reasonRepository;

    /**
     * @param Action\Context $context
     */
    public function __construct(
        Action\Context $context,
        \Aspl\Rmasystem\Api\ReasonRepositoryInterface $reasonRepository,
        \Aspl\Rmasystem\Api\Data\ReasonInterfaceFactory $reasonDataFactory
    ) {
        $this->reasonDataFactory = $reasonDataFactory;
        $this->reasonRepository = $reasonRepository;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Aspl_Rmasystem::save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
     
        $data = $this->getRequest()->getPostValue();
      
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $data['reason'] = strip_tags(isset($data['reason'])?$data['reason']:'');
            $id = $this->getRequest()->getParam('id');
            if ($id !== null) {
                $reasonModel = $this->reasonRepository->getById((int)$id);
            } else {
                $reasonModel = $this->reasonDataFactory->create();
            }
            $reasonModel->setData($data);

            try {
                $model = $this->reasonRepository->save($reasonModel);
                $this->messageManager->addSuccess(__('You saved this Reason.'));
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the reason.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
