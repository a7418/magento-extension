<?php
namespace Aspl\Rmasystem\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
class addressById extends Action
{
   
    protected $resultJsonFactory;

    private $addressRepository; 
    
    public function __construct(
        Context $context,
         AddressRepositoryInterface $addressRepository,
        JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->addressRepository = $addressRepository;
      }

 
    public function execute()
      {
        $post=$this->getRequest()->getParams();
        $addressId=$post['id'];
        // print_r($addressId);
        // die();
        $result = $this->resultJsonFactory->create();
        $addressData = $this->addressRepository->getById($addressId);
        $street=implode($addressData->getStreet([0]));
        $countryId=$addressData->getCountryId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $country = $objectManager->create('\Magento\Directory\Model\Country')->load($countryId)->getName();
        $regionId=$addressData->getRegionId();
        $region = $objectManager->create('Magento\Directory\Model\Region')
                        ->load($regionId)->getName();
        
       $address = array(['editaddressId'=>$addressData->getId().",".$addressData->getFirstname().",".$addressData->getLastname().",". $street.",".$region.",".$country.",".$addressData->getCity().",".$addressData->getTelephone().",".$addressData->getPostcode()]);
          return $result->setData($address);
          
      }
}
// $country.",".$addressData->getCity().",".$addressData->getTelephone().",".$addressData->getPostcode()