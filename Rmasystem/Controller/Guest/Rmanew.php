<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Controller\Guest;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Session\SessionManager;

class Rmanew extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Session
     */
    protected $session;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    protected $helper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry,
        SessionManager $session,
        \Aspl\Rmasystem\Helper\Data $helper,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->session = $session;
        $this->helper = $helper;
        $this->_messageManager = $messageManager;
        parent::__construct($context);
    }

    /**
     * Create New Guest Rma
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
         //if module not activated
         $isModuleEnable = $this->helper->getConfigData('enable');
        if (!$isModuleEnable) {
            $this->_messageManager->addWarning(__('Requested url not found.'));
            $this->_redirect('/');
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultPage = $this->resultPageFactory->create();
        if ($this->helper->isCustomerLoggedIn()) {
            return $resultRedirect->setPath('*');
        }
        $guestSession = $this->session->getGuestData();
        if ($guestSession['email'] == '') {
            $this->messageManager->addError(
                __('You are not authorized to Create RMA.')
            );
            return $resultRedirect->setPath('*/guest/login');
        }
        $this->coreRegistry->register('guest_data', $guestSession);
        return $resultPage;
    }
}
