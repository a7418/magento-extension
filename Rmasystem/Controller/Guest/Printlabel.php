<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Controller\Guest;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Session\SessionManager;

class Printlabel extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Session
     */
    protected $session;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    protected $shippingLabel;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Aspl\Rmasystem\Model\ShippinglabelFactory $shippingLabel,
        SessionManager $session
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->session = $session;
        $this->shippingLabel = $shippingLabel;
        parent::__construct($context);
    }

    /**
     * Print Guest rma.
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->get(\Aspl\Rmasystem\Model\Allrma::class)->load($id);
        $resultPage->getConfig()->getTitle()->set(__('Pre Shipping Label'));
        $guestSession = $this->_objectManager->create(\Magento\Framework\Session\SessionManager::class)->getGuestData();
        if ($guestSession['email'] == '') {
            $this->messageManager->addError(
                __('You are not authorized to print this RMA.')
            );
            return $resultRedirect->setPath('*/guest/login');
        }
        $shippinig_label_id = $model->getShippingLabel();
        $collection = $this->shippingLabel->create()->getCollection()
                            ->addFieldToFilter('id', $shippinig_label_id);
        if (!$collection->getSize()) {
            $this->messageManager->addError(
                __('Shipping label is not available.')
            );
            return $this->resultRedirectFactory->create()->setPath(
                'rmasystem/guest/rmaview',
                ['id'=>$id, '_secure' => $this->getRequest()->isSecure()]
            );
        }
        $this->coreRegistry->register('guest_data', $guestSession);
        return $resultPage;
    }
}
