<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Controller\Guest;

class Filter extends \Aspl\Rmasystem\Controller\Index\Filter
{
    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        $this->_objectManager->create(
            \Magento\Framework\Session\SessionManager::class
        )->unsGuestFilterData();

        $data = $this->getRequest()->getPost();

        $this->_objectManager->create(
            \Magento\Framework\Session\SessionManager::class
        )->setGuestFilterData($data);
        return $resultPage;
    }
}
