<?php

namespace Aspl\Rmasystem\Api;

interface ApplyFilterInterface
{
    /**
     * Returns selected order detail
     *
     * @api
     * @param string $orderId
     * @param string $price
     * @param string $date
     * @return string.
     */
    public function applyFilter($orderId = 0, $price = null, $date = null);
}
