<?php

namespace Aspl\Rmasystem\Api;

/**
 * Rma reason CRUD interface
 * @api
 */
interface ReasonRepositoryInterface
{
    /**
     * Save Reason.
     *
     * @param Aspl\Rmasystem\Api\Data\ReasonInterface $reason
     * @return Aspl\Rmasystem\Api\Data\ReasonInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If a RMA reason ID is sent but it does not exist
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Aspl\Rmasystem\Api\Data\ReasonInterface $reason);

    /**
     * Get reason by rma ID.
     *
     * @param int $id
     * @return Aspl\Rmasystem\Api\Data\ReasonInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If $rmaId is not found
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve rma reason list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Aspl\Rmasystem\Api\Data\ReasonSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete rma reason.
     *
     * @param Aspl\Rmasystem\Api\Data\ReasonInterface $group
     * @return bool true on success
     * @throws \Magento\Framework\Exception\StateException If rma reason cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Aspl\Rmasystem\Api\Data\ReasonInterface $group);

    /**
     * Delete rma reason by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException If rma reason cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
