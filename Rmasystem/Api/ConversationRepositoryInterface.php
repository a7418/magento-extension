<?php

namespace Aspl\Rmasystem\Api;

/**
 * Rma Conversation CRUD interface
 * @api
 */
interface ConversationRepositoryInterface
{
    /**
     * Save Conversation.
     *
     * @param Aspl\Rmasystem\Api\Data\ConversationInterface $conversation
     * @return Aspl\Rmasystem\Api\Data\ConversationInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Aspl\Rmasystem\Api\Data\ConversationInterface $conversation);

    /**
     * Get Conversation by ID.
     *
     * @param int $id
     * @return Aspl\Rmasystem\Api\Data\ConversationInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If $rmaId is not found
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve conversation list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Aspl\Rmasystem\Api\Data\ConversationSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete conversation.
     *
     * @param Aspl\Rmasystem\Api\Data\ConversationInterface $group
     * @return bool true on success
     * @throws \Magento\Framework\Exception\StateException If rma cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Aspl\Rmasystem\Api\Data\ConversationInterface $group);

    /**
     * Delete conversation by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException If rma cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
