<?php

namespace Aspl\Rmasystem\Api;

/**
 * Rma item CRUD interface
 * @api
 */
interface RmaitemRepositoryInterface
{
    /**
     * Save RMA item.
     *
     * @param Aspl\Rmasystem\Api\Data\RmaitemInterface $rmaItem
     * @return Aspl\Rmasystem\Api\Data\RmaitemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If a RMA ID is sent but the rma does not exist
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Aspl\Rmasystem\Api\Data\RmaitemInterface $rmaItem);

    /**
     * Get RMA item by ID.
     *
     * @param int $id
     * @return Aspl\Rmasystem\Api\Data\RmaitemInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If $rmaId is not found
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve rma item list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Aspl\Rmasystem\Api\Data\RmaitemSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete rma item.
     *
     * @param Aspl\Rmasystem\Api\Data\RmaitemInterface $group
     * @return bool true on success
     * @throws \Magento\Framework\Exception\StateException If rma cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Aspl\Rmasystem\Api\Data\RmaitemInterface $rmaItem);

    /**
     * Delete RMA item by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException If rma cannot be deleted
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
