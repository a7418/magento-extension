<?php

namespace Aspl\Rmasystem\Api\Data;

interface ReasonInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID            = 'id';
    const REASON        = 'reason';
    const STATUS        = 'status';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get Rma Reason
     *
     * @return string
     */
    public function getReason();

    /**
     * Get Status
     *
     * @return boolen
     */
    public function getStatus();
    /**
     * Set ID
     *
     * @param int $id
     * @return \Aspl\Rmasystem\Api\Data\ReasonInterface
     */
    public function setId($id);

    /**
     * Set Rma Reason
     *
     * @param string $reason
     * @return \Aspl\Rmasystem\Api\Data\ReasonInterface
     */
    public function setReason($reason);

    /**
     * Set Status
     *
     * @param int $status
     * @return \Ashsmith\Blog\Api\Data\ReasonInterface
     */
    public function setStatus($status);
}
