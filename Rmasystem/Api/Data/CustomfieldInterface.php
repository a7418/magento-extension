<?php

namespace Aspl\Rmasystem\Api\Data;

interface CustomfieldInterface
{
    const ENTITY_ID    = 'id';
    public function getId();
    public function setId($id);
}
