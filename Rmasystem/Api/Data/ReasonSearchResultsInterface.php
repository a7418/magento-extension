<?php


namespace Aspl\Rmasystem\Api\Data;

/**
 * Interface for rma reason search results.
 * @api
 */
interface ReasonSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get reason list.
     *
     * @return \Aspl\Rmasystem\Api\Data\ReasonInterface[]
     */
    public function getItems();

    /**
     * Set reason list.
     *
     * @api
     * @param \Aspl\Rmasystem\Api\Data\ReasonInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
