<?php


namespace Aspl\Rmasystem\Api\Data;

/**
 * Interface for returned items search results.
 * @api
 */
interface RmaitemSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get returned items list.
     *
     * @return \Aspl\Rmasystem\Api\Data\RmaitemInterface[]
     */
    public function getItems();

    /**
     * Set returned items list.
     *
     * @api
     * @param \Aspl\Rmasystem\Api\Data\RmaitemInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
