<?php

namespace Aspl\Rmasystem\Api\Data;

interface FieldvalueInterface
{
    const ENTITY_ID    = 'id';
    public function getId();
    public function setId($id);
}
