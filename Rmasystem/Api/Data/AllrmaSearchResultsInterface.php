<?php


namespace Aspl\Rmasystem\Api\Data;

/**
 * Interface for all rma search results.
 * @api
 */
interface AllrmaSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get all rma list.
     *
     * @return \Aspl\Rmasystem\Api\Data\AllrmaInterface[]
     */
    public function getItems();

    /**
     * Set all rma list.
     *
     * @api
     * @param \Aspl\Rmasystem\Api\Data\AllrmaInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
