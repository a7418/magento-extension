<?php

namespace Aspl\Rmasystem\Api\Data;

/**
 * Interface for rma conversation search results.
 * @api
 */
interface ConversationSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get conversation list.
     *
     * @return \Aspl\Rmasystem\Api\Data\ConversationInterface[]
     */
    public function getItems();

    /**
     * Set conversation list.
     *
     * @api
     * @param \Aspl\Rmasystem\Api\Data\ConversationInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
