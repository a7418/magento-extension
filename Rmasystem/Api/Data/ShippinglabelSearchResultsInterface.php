<?php

namespace Aspl\Rmasystem\Api\Data;

/**
 * Interface for rma shipping label search results.
 * @api
 */
interface ShippinglabelSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get shipping label list.
     *
     * @return \Aspl\Rmasystem\Api\Data\ShippingLabelInterface[]
     */
    public function getItems();

    /**
     * Set shipping label list.
     *
     * @api
     * @param \Aspl\Rmasystem\Api\Data\ShippingLabelInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
