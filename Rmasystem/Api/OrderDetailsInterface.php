<?php

namespace Aspl\Rmasystem\Api;

interface OrderDetailsInterface
{
    /**
     * Returns selected order detail
     *
     * @api
     * @param int $orderId
     * @return string.
     */
    public function getDetails($orderId);
}
