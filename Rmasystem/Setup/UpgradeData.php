<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;

    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'rma_allowed', [
            'type'              => 'int',
            'label'             => 'Can Generate RMA?',
            'note'              => 'products belonging to this category can generate rma',
            'input'             => 'select',
            'source'            => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
            'visible'           => true,
            'default'           => '1',
            'required'          => false,
            'global'            => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'group'             => 'General Information',
            "searchable"        => false,
            "filterable"        => true,
            "comparable"        => false,
            'visible_on_front'  => false,
            'unique'            => false
        ]);
         $eavSetup->addAttribute(\Magento\Catalog\Model\Category::ENTITY, 'rma_return_days', [
            'type'              => 'varchar',
            'label'             => 'Rma Return Days',
            'note'              => 'products belonging to this category enter rma return days',
            'input'             => 'text',
            // 'source'            => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
            'visible'           => true,
            'default'           => '',
            'required'          => false,
            'global'            => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'group'             => 'General Information',
            "searchable"        => false,
            "filterable"        => true,
            "comparable"        => false,
            'visible_on_front'  => false,
            'unique'            => false
        ]);
           $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'return_type');
        $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, 'returntype', [
            'group' => 'Product Details',
            'type' => 'int',
            'backend' => '',
            'frontend' => '',
            'sort_order' => 210,
            'label' => 'Return type',
            'input' => 'select',
            'class' => '',
            'source' => 'Aspl\Rmasystem\Model\ReturnType\Source\IsActive',
            'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'user_defined' => false,
            'default' => '',
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => true,
            'apply_to' => ''
        ]);
        $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, 'returnproduct', [
            'group' => 'Product Details',
            'type' => 'int',
            'sort_order' => 200,
            'backend' => '',
            'frontend' => '',
            'label' => 'Available for Return/Exchange',
            'input' => 'boolean',
            'class' => '',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
            'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'user_defined' => false,
            'default' => '',
            'searchable' => false,
            'filterable' => false,
            'comparable' => false,
            'visible_on_front' => false,
            'used_in_product_listing' => true,
            'unique' => false,
            'apply_to' => ''
        ]);
       
    }
}
