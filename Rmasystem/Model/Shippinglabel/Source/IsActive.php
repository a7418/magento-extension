<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Model\Shippinglabel\Source;

class IsActive implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Aspl\Rmasystem\Model\Shippinglabel
     */
    protected $shippinglabel;

    /**
     * Constructor
     *
     * @param \Aspl\Rmasystem\Model\Shippinglabel $shippinglabel
     */
    public function __construct(\Aspl\Rmasystem\Model\Shippinglabel $shippinglabel)
    {
        $this->shippinglabel = $shippinglabel;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->shippinglabel->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
