<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Model\ResourceModel;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Aspl\Rmasystem\Model\ResourceModel\Shippinglabel\Collection;

/**
 * Rma shippingLabel CRUD class
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ShippingLabelRepository implements \Aspl\Rmasystem\Api\ShippingLabelRepositoryInterface
{
    /**
     * @var \Aspl\Rmasystem\Model\ShippinglabelFactory
     */
    protected $shippingLabelFactory;

    /**
     * @var \Aspl\Rmasystem\Api\Data\ShippinglabelInterfaceFactory
     */
    protected $shippingLabelDataFactory;

    /**
     * @var \Aspl\Rmasystem\Model\ResourceModel\Shippinglabel
     */
    protected $shippingLabelResourceModel;

    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Aspl\Rmasystem\Api\Data\ShippinglabelSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @param \Magento\Customer\Model\GroupFactory $groupFactory
     * @param \Magento\Customer\Api\Data\GroupInterfaceFactory $groupDataFactory
     * @param \Magento\Customer\Model\ResourceModel\Group $groupResourceModel
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Customer\Api\Data\GroupSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Tax\Api\TaxClassRepositoryInterface $taxClassRepositoryInterface
     * @param \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor
     */
    public function __construct(
        \Aspl\Rmasystem\Model\ShippinglabelFactory $shippingLabelFactory,
        \Aspl\Rmasystem\Api\Data\ShippinglabelInterfaceFactory $shippingLabelDataFactory,
        \Aspl\Rmasystem\Model\ResourceModel\Shippinglabel $shippingLabelResourceModel,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Aspl\Rmasystem\Api\Data\ShippinglabelSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->shippingLabelFactory = $shippingLabelFactory;
        $this->shippingLabelDataFactory = $shippingLabelDataFactory;
        $this->shippingLabelResourceModel = $shippingLabelResourceModel;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Aspl\Rmasystem\Api\Data\ShippinglabelInterface $shippingLabel)
    {
        /** @var \Aspl\Rmasystem\Model\Shippinglabel $shippingLabelModel */
        $shippingLabelModel = null;
        if ($shippingLabel->getId() || (string)$shippingLabel->getId() === '0') {
            $shippingLabelModel = $this->shippingLabelFactory->create()->load($shippingLabel->getId());
            $groupDataAttributes = $this->dataObjectProcessor->buildOutputDataArray(
                $shippingLabel,
                \Aspl\Rmasystem\Api\Data\ShippinglabelInterface::class
            );
        } else {
            $shippingLabelModel = $this->shippingLabelFactory->create();
            $shippingLabelModel->setData($shippingLabel->getData());
        }
        try {
            $this->shippingLabelResourceModel->save($shippingLabelModel);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($e->getMessage() == (string)__('Could not save the record.')) {
                throw new InvalidTransitionException(__('Could not save the record.'));
            }
            throw $e;
        }

        $shippingLabelDataObject = $this->shippingLabelDataFactory->create()
            ->setData($shippingLabelModel->getData());
        return $shippingLabelDataObject;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($entityId)
    {
        $shippingLabel = $this->shippingLabelFactory->create();
        $this->shippingLabelResourceModel->load($shippingLabel, $entityId);
        
        return $shippingLabel;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);

        /** @var \Aspl\Rmasystem\Model\ResourceModel\Shippinglabel\Collection $collection */
        $collection = $this->shippingLabelFactory->create()->getCollection();

        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType(): 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Aspl\Rmasystem\Api\Data\ShippinglabelInterface[] $groups */
        $shippingLabels = [];
        /** @var \Aspl\Rmasystem\Model\Shippinglabel $shippingLabel */
        foreach ($collection as $shippingLabel) {
            /** @var \Magento\Rmasystem\Api\Data\ShippinglabelInterface $shippingLabelDataObject */
            $shippingLabelDataObject = $this->shippingLabelDataFactory->create()
                ->setData($shippingLabel->getData());
            $shippingLabels[] = $groupDataObject;
        }
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult->setItems($shippingLabels);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Aspl\Rmasystem\Api\Data\ShippinglabelInterface $shippingLabel)
    {
        try {
            $this->shippingLabelResourceModel->delete($shippingLabel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($shippingLabelId)
    {
        return $this->delete($this->getById($shippingLabelId));
    }
}
