<?php

namespace Aspl\Rmasystem\Model\ResourceModel;

class Allrma extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
   
    protected $_store = null;

    
    protected function _construct()
    {
        $this->_init('wk_rma', 'rma_id');
    }

    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        if (!is_numeric($value) && $field === null) {
            $field = 'rma_id';
        }

        return parent::load($object, $value, $field);
    }

  
    public function setStore($store)
    {
        $this->_store = $store;
        return $this;
    }

  
    public function getStore()
    {
        return $this->_storeManager->getStore($this->_store);
    }
}
