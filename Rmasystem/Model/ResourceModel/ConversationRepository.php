<?php
/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */
namespace Aspl\Rmasystem\Model\ResourceModel;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Aspl\Rmasystem\Model\ResourceModel\Conversation\Collection;

/**
 * Rma conversation CRUD class
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ConversationRepository implements \Aspl\Rmasystem\Api\ConversationRepositoryInterface
{
    /**
     * @var \Aspl\Rmasystem\Model\ConversationFactory
     */
    protected $conversationFactory;

    /**
     * @var \Aspl\Rmasystem\Api\Data\ConversationInterfaceFactory
     */
    protected $conversationDataFactory;

    /**
     * @var \Aspl\Rmasystem\Model\ResourceModel\Conversation
     */
    protected $conversationResourceModel;

    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Aspl\Rmasystem\Api\Data\ConversationSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @param \Magento\Customer\Model\GroupFactory $groupFactory
     * @param \Magento\Customer\Api\Data\GroupInterfaceFactory $groupDataFactory
     * @param \Magento\Customer\Model\ResourceModel\Group $groupResourceModel
     * @param \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor
     * @param \Magento\Customer\Api\Data\GroupSearchResultsInterfaceFactory $searchResultsFactory
     * @param \Magento\Tax\Api\TaxClassRepositoryInterface $taxClassRepositoryInterface
     * @param \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor
     */
    public function __construct(
        \Aspl\Rmasystem\Model\ConversationFactory $conversationFactory,
        \Aspl\Rmasystem\Api\Data\ConversationInterfaceFactory $conversationDataFactory,
        \Aspl\Rmasystem\Model\ResourceModel\Conversation $conversationResourceModel,
        \Magento\Framework\Reflection\DataObjectProcessor $dataObjectProcessor,
        \Aspl\Rmasystem\Api\Data\ConversationSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->conversationFactory = $conversationFactory;
        $this->conversationDataFactory = $conversationDataFactory;
        $this->conversationResourceModel = $conversationResourceModel;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Aspl\Rmasystem\Api\Data\ConversationInterface $conversation)
    {
        /** @var \Aspl\Rmasystem\Model\Conversation $conversationModel */
        $conversationModel = null;
        if ($conversation->getId() || (string)$conversation->getId() === '0') {
            $conversationModel = $this->conversationFactory->create()->load($conversation->getId());
            $groupDataAttributes = $this->dataObjectProcessor->buildOutputDataArray(
                $conversation,
                \Aspl\Rmasystem\Api\Data\ConversationInterface::class
            );
        } else {
            $conversationModel = $this->conversationFactory->create();
            $conversationModel->setData($conversation->getData());
        }
        try {
            $this->conversationResourceModel->save($conversationModel);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($e->getMessage() == (string)__('Could not save the record.')) {
                throw new InvalidTransitionException(__('Could not save the record.'));
            }
            throw $e;
        }

        $conversationDataObject = $this->conversationDataFactory->create()
            ->setData($conversationModel->getData());
        return $conversationDataObject;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($entityId)
    {
        $conversation = $this->conversationFactory->create();
        $this->conversationResourceModel->load($conversation, $entityId);
        if (!$conversation->getId()) {
            throw new NoSuchEntityException(__('Record with id "%1" does not exist.', $entityId));
        }
        return $conversation;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);

        /** @var \Aspl\Rmasystem\Model\ResourceModel\Conversation\Collection $collection */
        $collection = $this->conversationFactory->create()->getCollection();

        /** @var FilterGroup $group */
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType(): 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $sortOrders = $searchCriteria->getSortOrders();
        /** @var SortOrder $sortOrder */
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        } else {
            $collection->addOrder($field, 'ASC');
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());

        /** @var \Aspl\Rmasystem\Api\Data\ConversationInterface[] $groups */
        $conversations = [];
        /** @var \Aspl\Rmasystem\Model\Conversation $conversation */
        foreach ($collection as $conversation) {
            /** @var \Magento\Rmasystem\Api\Data\ConversationInterface $conversationDataObject */
            $conversationDataObject = $this->conversationDataFactory->create()
                ->setData($conversation->getData());
            $conversations[] = $groupDataObject;
        }
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult->setItems($conversations);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Aspl\Rmasystem\Api\Data\ConversationInterface $conversation)
    {
        try {
            $this->conversationResourceModel->delete($conversation);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($conversationId)
    {
        return $this->delete($this->getById($conversationId));
    }
}
