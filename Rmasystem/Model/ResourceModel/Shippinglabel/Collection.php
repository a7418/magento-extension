<?php

namespace Aspl\Rmasystem\Model\ResourceModel\Shippinglabel;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
   
    protected $_idFieldName = 'id';
    
    protected function _construct()
    {
        $this->_init(
            \Aspl\Rmasystem\Model\Shippinglabel::class,
            \Aspl\Rmasystem\Model\ResourceModel\Shippinglabel::class
        );
        $this->_map['fields']['id'] = 'main_table.id';
    }
}
