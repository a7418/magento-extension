<?php

namespace Aspl\Rmasystem\Model\ReturnType\Source;

class IsActive extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource 
{
    public function getAllOptions() {
        if ($this->_options === null) {
            $this->_options = [
                ['label' => __('--Select--'), 'value' => ''],
                ['label' => __('Refundable'), 'value' => 1],
                ['label' => __('Exchange'), 'value' => 2]
            ];
        }
        return $this->_options;
    }
}
