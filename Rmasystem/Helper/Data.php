<?php

/**
 * Aspl Software.
 *
 * @category  Aspl
 * @package   Aspl_Rmasystem
 * @author    Aspl
 * @copyright Copyright (c) Aspl Software Private Limited (https://Aspl.com)
 * @license   https://store.Aspl.com/license.html
 */

namespace Aspl\Rmasystem\Helper;
use Magento\Directory\Model\Country;
use Magento\Directory\Model\CountryFactory;
use Magento\Framework\UrlInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\CatalogInventory\Model\Stock\Item;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * media sub folder
     * @var string
     */
    protected $subDir = 'Aspl/rmasystem/RMA';

    protected $labelSubDir = 'Aspl/rmasystem/shippinglabel';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    protected $_customFieldFactory;

    protected $_fieldvalue;

    protected $_objectManager;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    public $countryFactory;
    /**
     * Escaper
     *
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    protected $directoryBlock;

    protected $_urlBuilder;

    protected $scopeConfig;
    /**
     * __construct
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Aspl\Rmasystem\Model\CustomfieldFactory $customfield
     * @param \Aspl\Rmasystem\Model\FieldvalueFactory $fieldvalue
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\Escaper $_escaper
     * @param \Magento\Framework\Serialize\SerializerInterface $serializerInterface
     * @param Filesystem $fileSystem
     * @param HttpContext $httpContext
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        CountryFactory $countryFactory,
         \Magento\Framework\UrlInterface $_urlBuilder,
        \Magento\Directory\Block\Data $directoryBlock,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Aspl\Rmasystem\Model\CustomfieldFactory $customfield,
        \Aspl\Rmasystem\Model\FieldvalueFactory $fieldvalue,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Escaper $_escaper,
        StockItemRepository $stockItemRepository,
        \Magento\Framework\Serialize\SerializerInterface $serializerInterface,
        Filesystem $fileSystem,
        OrderRepositoryInterface $orderRepository,
        \Magento\Catalog\Model\CategoryFactory $catFactory,
        Item $stockItem,
        HttpContext $httpContext,
        // \Magento\Framework\App\Config\ScopeConfigInterface $
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryFactory,
        \Aspl\Rmasystem\Model\RmaitemFactory $rmaItemFactory,
        \Magento\CatalogInventory\Api\StockStateInterface $stockState
    ) {
        parent::__construct($context);
         $this->scopeConfig = $context->getScopeConfig();
        $this->countryFactory = $countryFactory;
        $this->_escaper = $_escaper;
        $this->stockState = $stockState;
        $this->rmaItemFactory = $rmaItemFactory;
        $this->catFactory = $catFactory;
        $this->stockItemRepository = $stockItemRepository;
        $this->stockItem = $stockItem;
        $this->storeManager = $storeManager;
        $this->fileSystem = $fileSystem;
        $this->serializerInterface = $serializerInterface;
        $this->_customFieldFactory = $customfield;
        $this->_fieldvalue = $fieldvalue;
        $this->_objectManager = $objectManager;
        $this->httpContext = $httpContext;
        $this->productFactory = $productFactory;
        $this->_orderRepository = $orderRepository;
        $this->categoryCollectionFactory = $categoryFactory;
        $this->directoryBlock = $directoryBlock;
        $this->_urlBuilder=$_urlBuilder;
    }

    /**
     * Retrieve information from carrier configuration.
     *
     * @param string $field
     *
     * @return void|false|string
     */
    public function getCountryAction()
    {
        return $this->_urlBuilder->getUrl('rmasystem/index/country');
    }
    public function getCountries()
    {
        $country = $this->directoryBlock->getCountryHtmlSelect();
        return $country;
    }
    public function getRegion(){
        $region = $this->directoryBlock->getRegionHtmlSelect();
        return $region;
    }
    public function getCountryName($countryId): string
    {
        $countryName = '';
        $country = $this->countryFactory->create()->loadByCode($countryId);
        if ($country) {
            $countryName = $country->getName();
        }
        return $countryName;
    }
     public function getAllowedStatus($field)
    {
           $path = 'rmasystem/parameter/allow_days' . $field;

        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }
    public function getConfigData($field)
    {
        $path = 'rmasystem/parameter/' . $field;

        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    /* Get category object */
    public function getCategory($categoryId)
    {
        $category = $this->catFactory->create()->load($categoryId);
        return $category;
    }

    /* Get parent category object  */
    public function getParentCategory($categoryId)
    {
        return $this->getCategory($categoryId)->getParentCategory();
    }

    /* Get parent category Id */
    public function getParentId($categoryId)
    {
        return $this->getCategory($categoryId)->getParentId();
    }

    /* Get all parent categories ids  */
    public function getParentIds($categoryId)
    {
        return $this->getCategory($categoryId)->getParentIds();
    }

    /**
     * get images base url
     *
     * @return string
     */
    public function getBaseDirRead()
    {
        return $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath($this->subDir . '/');
    }

    /**
     * get images base url
     *
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $this->subDir . '/';
    }

    /**
     * get images base url
     *
     * @return string
     */
    public function getLabelBaseUrl()
    {
        return $this->_urlBuilder
        ->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $this->labelSubDir . '/image/';
    }

    /**
     * get base image dir
     *
     * @return string
     */
    public function getBaseDir($lastRmaId)
    {
        return $this->fileSystem
            ->getDirectoryWrite(DirectoryList::MEDIA)
            ->getAbsolutePath(
                $this->subDir . '/' . $lastRmaId . '/image/'
            );
    }

    /**
     * get base image dir
     *
     * @return string
     */
    public function getConversationDir($lastRmaId)
    {
        return $this->fileSystem
            ->getDirectoryWrite(DirectoryList::MEDIA)
            ->getAbsolutePath(
                $this->subDir . '/conversation/' . $lastRmaId . '/'
            );
    }

    public function getBarcodeDir()
    {
        return $this->fileSystem
        ->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath($this->subDir . '/Barcodes/');
    }
    /**
     * get images base url
     *
     * @return string
     */
    public function getBarcodeBaseUrl()
    {
        return $this->_urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $this->subDir . '/Barcodes/';
    }

    /**
     * get images base url
     *
     * @return string
     */
    public function getConversationUrl($rmaId)
    {
        return $this->_urlBuilder->getBaseUrl(
            ['_type' => UrlInterface::URL_TYPE_MEDIA]
        ) . $this->subDir . '/conversation/' . $rmaId . '/';
    }

    /**
     * return getResolutionTypes array
     *
     * @param integer $status //invoice status
     * @return array
     */
    public function getResolutionTypes($status = 0, $shippmentStatus = 0)
    {
    

        if ($status == 1 && $shippmentStatus == 1) {
            return [
                ['value' => '3', 'label' => __('Cancel Items')]
            ];
        } elseif ($status == 2 && $shippmentStatus == 1) {
            return [
                ['value' => '0', 'label' =>  __('Refund')],
            ];
        } elseif ($status == 2 && $shippmentStatus == 2) {
            return [
                ['value' => '0', 'label' =>  __('Refund')],
                ['value' => '1', 'label' =>  __('Exchange')]
            ];
        } elseif ($shippmentStatus == 2 && $status == 1) {
            return [
                ['value' => '1', 'label' =>  __('Exchange')]
            ];
        } else {
            return [
                ['value' => '0', 'label' =>  __('Refund')],
                ['value' => '1', 'label' =>  __('Exchange')],
                ['value' => '3', 'label' => __('Cancel Items')]
            ];
        }
    }

    public function getDeliveryStatus($type)
    {

        $resoulution = [
           ['value' => '1', 'label' =>  __('Delivered')]
        ];
         // ['value' => '0', 'label' =>  __('Not Delivered')], 
        return $resoulution;
    }

    /**
     * Get Order Status Title
     *
     * @param int $status
     *
     * @return string
     */
    public function getRmaOrderStatusTitle($status)
    {
        if ($status ==  1) {
            $orderStatus =  __("Delivered");
        } else {
            $orderStatus =  __("Not Delivered");
        }
        return $orderStatus;
    }

    /**
     * Get RMA Status Title
     *
     * @param int $status
     * @param int $finalStatus
     *
     * @return string
     */
    public function getRmaStatusTitle($status, $finalStatus = 0)
    {
        if ($finalStatus == 0) {
            if ($status == 1) {
                $rmaStatus =  __("Processing");
            } elseif ($status == 2) {
                $rmaStatus =  __("Solved");
            } elseif ($status == 3) {
                $rmaStatus =  __("Declined");
            } elseif ($status == 4) {
                $rmaStatus =  __("Cancelled");
            } else {
                $rmaStatus =  __("Pending");
            }
        } else {
            if ($finalStatus == 1) {
                $rmaStatus =  __("Cancelled");
            } elseif ($finalStatus == 2) {
                $rmaStatus =  __("Declined");
            } elseif ($finalStatus == 3 || $finalStatus == 4) {
                $rmaStatus =  __("Solved");
            } else {
                $rmaStatus =  __("Pending");
            }
        }
        return $rmaStatus;
    }
    /**
     * Get RMA Status class
     *
     * @param int $status
     * @param int $finalStatus
     *
     * @return string
     */
    public function getRmaStatusClass($status, $finalStatus = 0)
    {
        if ($finalStatus == 0) {
            if ($status == 1) {
                $rmaStatus =  __("processing");
            } elseif ($status == 2) {
                $rmaStatus =  __("solve");
            } elseif ($status == 3) {
                $rmaStatus =  __("decline");
            } elseif ($status == 4) {
                $rmaStatus =  __("cancel");
            } else {
                $rmaStatus =  __("pending");
            }
        } else {
            if ($finalStatus == 1) {
                $rmaStatus =  __("cancel");
            } elseif ($finalStatus == 2) {
                $rmaStatus =  __("decline");
            } elseif ($finalStatus == 3 || $finalStatus == 4) {
                $rmaStatus =  __("solve");
            } else {
                $rmaStatus =  __("pending");
            }
        }
        return $rmaStatus;
    }
    /**
     * Get Seller's All Status
     *
     * @param int $status
     *
     * @return array
     */
    public function getAllStatus($resolutionType = 0)
    {
        if ($resolutionType == 3) {
            $allStatus = [
                0 => __('Pending'),
                5 => __('Declined'),
                6 => __('Item Cancelled')
            ];
        } elseif ($resolutionType == 0) {
            $allStatus = [
                0 => __('Pending'),
                1 => __('RMA Approved'),
                5 => __('Declined'),
                6 => __('Refund Initiated')
            ];
        } elseif ($resolutionType == 1) {
            $allStatus = [
                0 => __('Pending'),
                1 => __('RMA Approved'),
                2 => __('Received Package'),
                3 => __('Dispatched Package'),
                5 => __('Declined'),
                6 => __('Solved')
            ];
        } else {
            $allStatus = [
                0 => __('Pending'),
                1 => __('RMA Approved'),
                5 => __('Declined'),
                6 => __('Solved')
            ];
        }
        return $allStatus;
    }

    public function getAdminStatusTitle($status, $resolutionType)
    {
        $allStatus = $this->getAllStatus($resolutionType);
        return $allStatus[$status];
    }

    /**
     * get custom form fields
     *
     * @return collection
     */
    public function getFields()
    {
        $collection = $this->_customFieldFactory->create()
            ->getCollection()
            ->addFieldToFilter('status', 1);
        $collection->setOrder('sort', 'ASC');
        return $collection;
    }

    public function getFieldData($rma_id)
    {
        $collection = $this->_fieldvalue->create()->getCollection()
            ->addFieldToFilter('rma_id', $rma_id);
        $joinTable = $this->_objectManager->create(\Magento\Framework\App\ResourceConnection::class)
            ->getTableName("wk_rma_customfield");
        $sql = "cf.id = main_table.field_id";
        $collection->getSelect()->join($joinTable . ' as cf', $sql, ['input_type', 'label', 'select_option']);
        return $collection;
    }

    public function getText($data)
    {

        if ($data->getRequired() == 1) {
            $required = 'required clear-section';
        } else {
            $required = '';
        }
        $getFrontEndClass = $data->getFrontendClass();
        $requiredClass =  $required . ' ' . $getFrontEndClass;
        $field = "<div class='field " . $required . "'><label class='label'><span>" .
            $this->escapeHtml($data->getLabel()) . ":</span></label>";
        $field = $field . "<div class='control'><input type ='text' value='' class='" . $requiredClass . "' name='" .
            $this->escapeHtml($data->getInputname()) . "'></div></div>";
        return $field;
    }

    public function getTextarea($data)
    {
        if ($data->getRequired() == 1) {
            $required = 'required clear-section';
        } else {
            $required = '';
        }
        $field = "<div class='field " . $required . "'><label class='label'><span>" .
            $this->escapeHtml($data->getLabel()) . ":</span></label>";
        $field = $field . "<div class='control'><textarea name='" .
            $this->escapeHtml($data->getInputname()) . "' class='" . $required . "'></textarea></div></div>";
        return $field;
    }

    public function getSelect($data)
    {
        if ($data->getRequired() == 1) {
            $required = 'required clear-section';
        } else {
            $required = '';
        }
        $options = explode(",", $data->getSelectOption());
        $value = '<option value="">' . __('Select') . '</option>';
        foreach ($options as $key) {
            $tmp = explode("=>", $key);
            if (count($tmp) == 2) {
                $value = $value . "<option value=" . $this->escapeHtml($tmp[0]) . ">" .
                    $this->escapeHtml($tmp[1]) . "</option>";
            }
        }
        $field = "<div class='field " . $required . "'><label class='label'><span>" .
            $this->escapeHtml($data->getLabel()) . "</span></label>";
        $field = $field . "<div class='control'><select name='" .
            $this->escapeHtml($data->getInputname()) . "' class='select " . $required . "' >" .
            $value . "</select></div></div>";
        return $field;
    }

    public function getMultiselect($data)
    {
        if ($data->getRequired() == 1) {
            $required = 'required clear-section ';
        } else {
            $required = '';
        }
        $options = explode(",", $data->getSelectOption());
        $value = '';
        foreach ($options as $key) {
            $tmp = explode("=>", $key);
            if (count($tmp) == 2) {
                $value = $value . "<option value=" . $this->escapeHtml($tmp[0]) . ">" .
                    $this->escapeHtml($tmp[1]) . "</option>";
            }
        }
        $field = "<div class='field " . $required . "'><label class='label'><span>" .
            $this->escapeHtml($data->getLabel()) . "</span></label>";
        $field = $field . "<div class='control'><select multiple name='" .
            $this->escapeHtml($data->getInputname()) . "[]' class='" . $required . "wk-rma-multiple' size='" .
            count($options) . "'>" . $value . "</select></div></div>";
        return $field;
    }

    public function getRadio($data)
    {
        if ($data->getRequired() == 1) {
            $required = 'required clear-section';
        } else {
            $required = '';
        }
        $options = explode(",", $data->getSelectOption());
        $value = '<p></p>';
        foreach ($options as $key) {
            $tmp = explode("=>", $key);
            if (count($tmp) == 2) {
                $value = $value . "<input type='radio' id='refund' class='" . $required . "' name='" .
                    $this->escapeHtml($data->getInputname()) . "' value=" . $this->escapeHtml($tmp[0]) . "><span>" .
                    $this->escapeHtml($tmp[1]) . "</span></br>";
            }
        }
        $field = "<div class='field " . $required . "'><label class='label'><span>" .
            $this->escapeHtml($data->getLabel()) . "</span></label>";
        $field = $field . $value . "</div>";
        return $field;
    }

    public function getCheckbox($data)
    {
        if ($data->getRequired() == 1) {
            $required = 'required clear-section';
        } else {
            $required = '';
        }
        $options = explode(",", $data->getSelectOption());
        $value = '<p></p>';
        foreach ($options as $key) {
            $tmp = explode("=>", $key);
            if (count($tmp) == 2) {
                $value = $value . "<input type='checkbox' class='" . $required . "' name='" .
                    $this->escapeHtml($data->getInputname()) . "[]' value=" .
                    $this->escapeHtml($tmp[0]) . "><span>" . $this->escapeHtml($tmp[1]) . "</span></br>";
            }
        }
        $field = "<div class='field " . $required . "'><label class='label'><span>" .
            $this->escapeHtml($data->getLabel()) . "</span></label>";
        $field = $field . $value . "</div>";
        return $field;
    }

    public function escapeHtml($string)
    {
        return $this->_escaper->escapeHtml($string);
    }

    /**
     * Check if customer is logged in
     *
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return (bool)$this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    /**
     * function to check is category allowed for rma
     *
     * @param [array] $catIds
     * @return boolean
     */
    public function isCategoryAllowed($catIds)
    {
        $flag = true;
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $data = [];
        foreach ($catIds as $cat) {
            $category = $_objectManager->create(\Magento\Catalog\Model\Category::class)->load($cat);
            $data["is_allowed"][] = ($category->getRmaAllowed() == "") ? 2 : $category->getRmaAllowed();
           
            $parentIds = $this->getParentIds($cat);
            foreach ($parentIds as $parentId) {
                $getParentCategory = $this->getCategory($parentId);
                $data["parent_is_allowed"][] = ($getParentCategory->getRmaAllowed() == "") ?
                 2:$getParentCategory->getRmaAllowed();
            }
        }
       
        if (in_array(0, $data['is_allowed']) || in_array(0, $data['parent_is_allowed'])) {
            $flag = false;
        }
        return $flag;
    }
    /**
     *
     * @param array $data
     * @return serialize data
     */
    public function getSerializeData($data)
    {
        return $this->serializerInterface->serialize($data);
    }
    /**
     * updateProductQty
     *
     * @param array $post
     * @return void
     */
    public function updateProductQty($post)
    {

        try {
            $orderId = $post['order_id'];
            $rma_id = $post['rma_id'];
            $rmaData = $this->rmaItemFactory->create()
                ->getCollection()
                ->addFieldToFilter('rma_id', $rma_id);
            $data = [];
            foreach ($rmaData->getData() as $val) {
                $data[$val['item_id']] = $val['qty'];
            }
            $order = $this->_orderRepository->get($orderId);

            $item_data = [];
            $count = 0;

            foreach ($order->getAllVisibleItems() as $item) {
                if (array_key_exists($item->getItemId(), $data)) {
                    $product = $this->productFactory->create()->load($item->getProductId());
                    $stockItem = $this->stockItem->load($product->getId(), 'product_id');
                    $stockItem->setQty($stockItem->getQty() + $data[$item->getItemId()]);
                    $stockItem->save();
                }
            }
            $result['error'] = 0;
        } catch (\Exception $e) {
            $result['error'] = 1;
            $result['msg'] = $e->getMessage();
        }
        return $result;
    }
    public function addToStockData($post)
    {
      
        $result = [];
        $orderId = $post['order_id'];
        $order = $this->_orderRepository->get($orderId);
        $itemId = [];
        $stockData = $post['add_to_stock'];
        try {
            foreach ($stockData as $key => $val) {
                if (isset($val['is_add_to_stock']) && !empty($val['is_add_to_stock'])) {
                    $itemId[] = $key;
                }
            }
            foreach ($order->getAllVisibleItems() as $item) {

                if (in_array($item->getItemId(), $itemId)) {
                    $qty = isset($stockData[$item->getItemId()]['qty']) ? $stockData[$item->getItemId()]['qty'] : 0;
                    if ($item->getProductType() == "configurable") {
                        $childItems = $item->getChildrenItems();
                        $setStockData =  $this->setConfigurableProductStockItem($childItems, $item, $qty);
                        if ($setStockData['error']) {
                            $setStockData['error'] = 1;
                            $setStockData['msg'] = $setStockData['msg'];
                        }
                    } else {
                        $setStockData =   $this->setStockData($item, $qty);
                        if ($setStockData['error']) {
                            $setStockData['error'] = 1;
                            $setStockData['msg'] = $setStockData['msg'];
                        }
                    }
                }
            }
            $result['error'] = 0;
        } catch (\Exception $e) {
            $result['error'] = 1;
            $result['msg'] = $e->getMessage();
        }
        return $result;
    }
    public function setConfigurableProductStockItem($childItems, $item, $qty)
    {
        $result['error'] = 0;
        try {
            foreach ($childItems as $childItem) {
                $product = $this->productFactory->create()->load($childItem->getProduct()->getId());
                $stockItem = $this->stockItem->load($product->getId(), 'product_id');
                $stockItem->setQty($stockItem->getQty() + $qty);
                $stockItem->save();
                $rmaItemData = $this->rmaItemFactory->create()->load($item->getItemId(), 'item_id');
                $rmaItemData->setIsStockAdded(1);
                $rmaItemData->save();
            }
        } catch (\Exception $e) {
            $result['error'] = 1;
            $result['msg'] = $e->getMessage();
        }
        return $result;
    }

    public function setStockData($item, $qty)
    {
        $result['error'] = 0;
        try {
            $product = $this->productFactory->create()->load($item->getProductId());
            $stockItem = $this->stockItem->load($product->getId(), 'product_id');
            $stockItem->setQty($stockItem->getQty() + $qty);
            $stockItem->save();
            $rmaItemData = $this->rmaItemFactory->create()->load($item->getItemId(), 'item_id');
            $rmaItemData->setIsStockAdded(1);
            $rmaItemData->save();
        } catch (\Exception $e) {
            $result['error'] = 1;
            $result['msg'] = $e->getMessage();
        }
        return $result;
    }

    public function getRmaData($rma_id)
    {
        $rmaData = $this->rmaItemFactory->create()
            ->getCollection()
            ->addFieldToFilter('rma_id', $rma_id);
        return $rmaData;
    }
}
